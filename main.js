function myFunc() {
  let count = 0;
  function sum(num) {
    count += num;
    return count;
  }
  return sum;
}

const adder = myFunc();

const x = adder(3);
const y = adder(5);
const r = adder(20);

console.log(x, y, r);
